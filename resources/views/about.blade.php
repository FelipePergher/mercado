@extends('layouts.template')
@section('content')

<link href="{{ asset('styles/about.css') }}" rel="stylesheet">

<div class="container" style="width: 90%;">
    <div class="row">
        <h3>Sobre nós</h3>
    </div>
    <div class="row">
        <p style="text-align:justify; text-indent:4em;"> 
            O Mercado Fagundes, foi fundado em 1996 na cidade de Fraiburgo. A empresa nasce para suprir as necessidades em compras para seus clientes, sendo desta em várias partes do Brasil. Deste sua implantação, a empresa vem conquistando espaço no mercado com objetivo de se aprimorar continuamente, buscando comprometimento com a qualidade e bem estar de seus clientes. Para isso a empresa conta com uma equipe preparada e especializada que visa um melhor atendimento.
        </p>
    </div>
    <div class="row">
        <h2>Nossa Localização</h2>
    </div>
    <div class="row">
        <div id="map-canvas"></div>
    </div>
    

    <div class="row">
        <div class="col-lg-4">
            <h3>Contate-nos</h3>    
            <h5><i class="ion-android-call"></i> (49)3246-2525</h5>
            <h5><i class="ion-ios-email"></i> E-mail: atendimento@mercadofagundes.com.br</h5>
            <h5><i class="ion-ios-location"></i> Endereço: Rua Frei Rogério, 172 Fraiburgo - SC</h5>
        </div>

        <div class="col-lg-5">
            <h3 style="margin-left: 11px;"><i class="ion-android-time"></i> Horário de Atendimento</h3>
            <div class="col-lg-6">
                <h6> <strong><i class=ion-android-calendar></i> Segunda à Sexta</strong></h6>
                <h6>8:30h às 21:00h</h6>
                <h6>Sem fechar ao meio dia</h6>
            </div>
            <div class="col-lg-6">
                <h6> <strong> <i class=ion-android-calendar></i> Sábado </strong> </h6>
                <h6>8:30h às 19:00h</h6>
                <h6>Sem fechar ao meio dia</h6>
            </div>
        </div>

        <div class="col-lg-3" style="widht: 23%;">
            <h3>Conecte-se conosco</h3>
            <div class="row">
                <ul class="nav btn_social">
                    <li>
                        <div class="col-lg-3">
                            <a href="https://www.facebook.com/" class="button">
                                <i class="ion-social-facebook"></i>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="https://plus.google.com" class="button">
                                <i class="ion-social-googleplus"></i>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="https://www.twitter.com" class="button">
                                <i class="ion-social-twitter"></i>
                            </a>
                        </div>
                        <div class="col-lg-3"> 
                            <a href="https://www.youtube.com" class="button"> 
                                <i class="ion-social-youtube"></i>
                            </a>
                        </div>
                    </li>
                </ul>   
            </div>
        </div>
    </div>
    
    <div class="row" style="text-align:center; margin-top:20px;">
        <img src="img/Site-Blindado.png" height="35" width="120">
    </div>
    
</div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEChUjeaBEJ5NbQCeA5nBZ71-GIuO-RvY"></script>
<script type="text/javascript">
   function initialize() {
       var positionMap = new google.maps.LatLng(-27.0246939, -50.9106836);
       var mapOptions = {
           zoom: 15,
           center: positionMap,
           disableDefaultUI: true,
       }
   
       var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
   
       var marker = new google.maps.Marker({
           position: positionMap,
           map: map,
           title: 'Amanda'
       });
   
       $(window).resize(function() {
           google.maps.event.trigger(map, "resize");
           map.setCenter(positionMap);
       });
   }
   google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection