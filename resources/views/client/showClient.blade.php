@extends('layouts.template')
@section('content')
<link href="{{ asset('styles/indexClient.css') }}" rel="stylesheet">
           

<div class="margem">
<h2> <a href="{{ route('home') }}"><span class="ion-arrow-left-c"></span></a> Informações da Conta</h2>
<div class="panel panel-default">
    <div class="panel-body">
            <table class="table table-bordered table-striped table-responsive">
                <tr>
                    <th>Nome</th>
                    <td>{{$users->nome}}</td>
                </tr>
                <tr>
                    <th>Sobrenome</th>
                    <td>{{$users->sobrenome}}</td>
                </tr>
                <tr>
                    <th>CPF</th>
                    <td>{{$users->cpf}}</td>
                </tr>
                <tr>
                    <th>RG</th>
                    <td>{{$users->rg}}</td>
                </tr>
                <tr>
                    <th>Telefone</th>
                    <td>{{$users->telefone}}</td>
                </tr>
                <tr>
                    <th>Celular</th>
                    <td>{{$users->celular}}</td>
                </tr>
                <tr>
                    <th>Rua</th>
                    <td>{{$users->rua}}</td>
                </tr>
                <tr>
                    <th>Bairro</th>
                    <td>{{$users->bairro}}</td>
                </tr>
                <tr>
                    <th>Complemento</th>
                    <td>{{$users->complemento}}</td>
                </tr>
                <tr>
                    <th>Numero</th>
                    <td>{{$users->numero}}</td>
                </tr>
                <tr>
                    <th>CEP</th>
                    <td>{{$users->cep}}</td>
                </tr>
                <tr>
                    <th>Data de nascimento</th>
                    <td>{{$users->datanascimento}}</td>
                </tr>
                <tr>
                    <th>Função</th>
                    <td>{{$users->role}}</td>
                </tr>
            </table>

            <div class="row">
               
                <div class="col-md-6">
                    @if ($users->id == 1)
                        <a href="/client/{{$users->id}}/edit" class="btn btn-success pull-right disabled">
                            Editar cadastro
                        </a>
                    @else
                        <a href="/client/{{$users->id}}/edit" class="btn btn-success pull-right">
                            Editar cadastro
                        </a>
                    @endif
                </div>
               

                <div class="col-md-6 pull-left">
                    {{ Form::open(array('url' => 'client/' . $users->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('Excluir cadastro', array('class'=>'btn btn-danger', 'type'=>'submit'))}}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection