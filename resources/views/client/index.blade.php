@extends('layouts.template')
@section('content')


<link href="{{ asset('styles/indexClient.css') }}" rel="stylesheet">
<link href="{{ asset('styles/campoPesquisa.css') }}" rel="stylesheet">

<div class= "margem">
  <div class="row pesquisa">
    {{Form::open(array('route' => array('client.index'), 'method' => 'GET'))}}
      {{Form::label('nome', 'Nome do cliente:',['class' => 'col-lg-2 control-label', 'style' => 'margin-left:8%;'])}}
      {{Form::text('nome',"",['class' => 'col-lg-7', 'style' => 'border-radius:4px; width: 50%;'])}}
      {{Form::submit('Consultar', array('class' => 'btn btn-danger pull-left col-lg-1' ,'style' => 'margin-top:-5px; margin-left:2%'))}}
    {{ Form::close() }}
  </div>
  <hr/>
  <h2 class="titulo">Lista de Clientes</h2>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th class="colunaCategoria">Nome</th>
        <th class="colunaDescricao">Sobrenome</th>
        <th class="colunaDescricao">E-mail</th>
        <th class="colunaAcao">Telefone</th>
        <th class="colunaAcao">Celular</th>
        <th class="colunaAcao">Detalhes</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{$user->nome}}</td>
          <td>{{$user->sobrenome}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->telefone}}</td>
          <td>{{$user->celular}}</td>
          <td>
            <a href = "/client/{{$user->id}}" aria-label="Ver cliente">
                <span class="btn btn-danger ion-information buttonIndex" aria-hidden="true"></span>
            </a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection