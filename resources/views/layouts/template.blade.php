<!doctype html>
<html lang="{{ app()->getLocale() }}">  
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Fagundes</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Bootstrap core CSS     -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        
        <!-- Animation library for notifications   -->
        <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

        <!--  Light Bootstrap Table core CSS    -->
        <link href="{{ asset('css/light-bootstrap-dashboard.css') }}" rel="stylesheet">

        <link href="{{ asset('styles/site.css') }}" rel="stylesheet">

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        
        <link href="{{asset('css/ionicons.min.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-color="red">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="{{ route('home') }}" class="simple-text">
                            <img src="{{ asset('img/logo_branco.png') }}" height="110" width="110">
                            <h5>Mercado Fagundes</h5>
                        </a>
                    </div>
                    <ul class="nav" id="categorias">
                        <li id="alimentos">
                            <a href="{{ route('getProductsbyCategory','Alimentos') }}">
                                <i class="ion-soup-can"></i>
                                <p> Alimentos</p>
                            </a>
                        </li>

                        <li id="docesSalgados">
                            <a href="{{ route('getProductsbyCategory','Doces - Salgados') }}">
                                <i class="ion-pizza"></i>
                                <p>Doces - Salgados</p>
                            </a>
                        </li>

                        <li id="frutasVerduras">
                            <a href="{{ route('getProductsbyCategory','Frutas - Verduras') }}">
                                <i class="ion-ios-nutrition"></i>
                                <p>Frutas - Verduras</p>
                            </a>
                        </li>

                        <li id="higieneLimpeza">
                            <a href="{{ route('getProductsbyCategory','Higiene - Limpeza') }}">
                                <i class="ion-waterdrop"></i>
                                <p>Higiene - Limpeza</p>
                            </a>
                        </li>

                        <li id="padaria">
                            <a href="{{ route('getProductsbyCategory','Padaria') }}">
                                <i class="ion-coffee"></i>
                                <p>Padaria</p>
                            </a>
                        </li>

                        <li id="frios">
                            <a href="{{ route('getProductsbyCategory','Frios') }}">
                                <i class="ion-icecream"></i>
                                <p>Frios</p>
                            </a>
                        </li>

                        <li id="bebidas">
                            <a href="{{ route('getProductsbyCategory','Bebidas') }}">
                                <i class="ion-wineglass"></i>
                                <p>Bebidas</p>
                            </a>
                        </li>

                        <li id="diversos">
                            <a href="{{ route('getProductsbyCategory','Itens diversos') }}">
                                <i class="ion-ios-plus-outline"></i>
                                <p>Itens diversos</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        
                            <a class="navbar-brand" href="{{ route('home') }}">
                            <i class="ion-ios-home-outline"></i> Home</a>
                            <a class="navbar-brand" href="{{ route('about') }}">
                            <i class="ion-ios-information-outline"></i> Sobre</a>
                            
                        </div>
                        
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left" style="margin-left:5%">
                                <li style="align-content: center">
                                    {{Form::open(array('route' => array('getProductsbyName'), 'method' => 'GET'))}}
                                        {{Form::text('nome',null, ['style' => 'border-radius: 4px; width:350px', 'placeholder' => " O que você procura?"])}}
                                        {{Form::submit('Consultar',['class' => 'btn btn-danger'])}}
                                    {{Form::close()}}
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                @if (Auth::guest() == false && (Auth::user()->role == 'gerente' || Auth::User()->role == 'vendedor'))
                                    <li class="dropdown">
                                    
                                        <a href="#" class="dropdown-toggle ion-person" data-toggle="dropdown" aria-expanded="false">
                                            Administrativo<span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="/product"><i class="ion-pricetag"></i> Produtos</a>
                                            </li>
                                            <li>
                                                <a href= "{{route ('salesClient', 0)}}">
                                                    <i class="ion-arrow-graph-up-right"></i> Vendas
                                                </a>
                                            </li>
                                            <li>
                                                <a href= "{{route ('unpaidSales')}}">
                                                    <i class="ion-alert-circled"></i> Pagamentos Pendentes
                                                </a>
                                            </li>
                                            <li>
                                                <a href= "{{route ('undeliveredSales')}}">
                                                    <i class="ion-alert-circled"></i> Vendas não entregues
                                                </a>
                                            </li>
                                            @if (Auth::user()->role == 'gerente') 
                                                <li>
                                                    <a href="/client"> <i class="ion-ios-people"></i> Clientes</a>
                                                </li>
                        
                                                <li>
                                                    <a href="/user"><i class="ion-person-stalker"></i> Usuários</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                @endif
                                <li>
                                    <a href="{{ route('sale') }}"><span class="ion-ios-cart" style="font-size:20px"> </span></a>
                                </li>

                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}"><i class="ion-log-in"></i> Entrar</a></li>
                                <li><a href="{{ route('register') }}"><i class="ion-ios-compose-outline"></i> Registrar-se</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        {{ Auth::user()->nome }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href= "{{route ('showClient', Auth::user()->id)}}">
                                                <i class="ion-ios-information-outline"></i> Minha conta
                                            </a>
                                        </li>
                                        <li>
                                            <a href= "{{route ('salesClient', Auth::user()->id)}}">
                                                <i class="ion-ios-information-outline"></i> Meus pedidos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <i class="ion-log-out"></i> Sair
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            </ul>   
                        </div>
                    </div>
                    
                </nav>
                @yield('content')
            </div>
        </div>

   <!-- <footer class="footer navbar-fixed-bottom footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="#">
                            Home
                        </a>
                    </li>
                </ul>
            </nav>
            <p class="copyright pull-right">
                &copy; 2017
            </p>
        </div>
    </footer>-->


    </body>

    <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
     
    <script src="{{ asset('/js/jquery.maskMoney.js') }}"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ asset('js/bootstrap-checkbox-radio-switch.js') }}"></script>

	<!--  Charts Plugin -->
    <script src="{{ asset('js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="{{ asset('js/light-bootstrap-dashboard.js') }}"></script>

    <script src="{{ asset('scripts/site.js') }}"></script>
    <script src="{{ asset('scripts/mask.js') }}"></script>


    @yield('script')
    <script type="text/javascript">
        var url = window.location.pathname.split("/").pop();
        if (url === "products") $('#allProducts').addClass('active');
        else if (url === "Alimentos") $('#alimentos').addClass('active');
        else if (url === "Doces%20-%20Salgados") $('#docesSalgados').addClass('active');
        else if (url === "Frutas%20-%20Verduras") $('#frutasVerduras').addClass('active');
        else if (url === "Higiene%20-%20Limpeza") $('#higieneLimpeza').addClass('active');
        else if (url === "Padaria") $('#padaria').addClass('active');
        else if (url === "Frios") $('#frios').addClass('active');
        else if (url === "Bebidas") $('#bebidas').addClass('active');
        else if (url === "Itens%20diversos") $('#diversos').addClass('active');
    </script>
</html>
