@extends('layouts.template')
@section('content')
<link href="{{ asset('styles/indexUsuario.css') }}" rel="stylesheet">
<link href="{{ asset('styles/campoPesquisa.css') }}" rel="stylesheet">


@if (session('sucesso'))
    <div class="ion-checkmark-circled alert alert-success">
        {{ session('sucesso') }}
    </div>
@endif

@if (session('erro'))
<div class="ion-alert-circled alert alert-danger">
    {{ session('erro') }}
</div>
@endif

<div class="margem">
  <a href="{{ route('user.create') }}" class="btn btn-danger">
      Novo usuário
  </a>
  
  <hr/>
  <div class="row pesquisa">
      {{Form::open(array('route' => array('user.index'), 'method' => 'GET'))}}
        {{Form::label('nome', 'Nome do usuário:',['class' => 'col-lg-2 control-label', 'style' => 'margin-left:8%;'])}}
        {{Form::text('nome',"",['class' => 'col-lg-7', 'style' => 'border-radius:4px; width: 50%;'])}}
        {{Form::submit('Consultar', array('class' => 'btn btn-danger pull-left col-lg-1' ,'style' => 'margin-top:-5px; margin-left:2%'))}}
      {{ Form::close() }}
  </div>
  <hr/>
  <h2 class="titulo">Lista de Usuários</h2>  
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Sobrenome</th>
        <th>E-mail</th>
        <th>Função</th>
        <th>Telefone</th>
        <th>Celular</th>
        <th>Detalhes</th>
        <th>Editar</th>
        <th>Remover</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{$user->nome}}</td>
          <td>{{$user->sobrenome}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->role}}</td>
          <td>{{$user->telefone}}</td>
          <td>{{$user->celular}}</td>

          <td>
            <a href = "{{route('user.show',$user->id)}}" aria-label="Ver usuario">
                <span class="btn btn-danger ion-information buttonIndex" aria-hidden="true"></span>
            </a>
          </td>
          
          @if ($user->nome == "admin" && $user->sobrenome == "admin")
            <td>
              <a href = "#" aria-label="Editar usuario">
                  <span class="btn btn-danger ion-edit buttonIndex disabled" aria-hidden="true"></span>
              </a>
            </td>
          @else
          
          <td>
              <a href = "{{route('editUser',$user->id)}}" aria-label="Editar usuario">
                  <span class="btn btn-danger ion-edit buttonIndex" aria-hidden="true"></span>
              </a>
            </td>
          @endif
          <td>
            {{ Form::open(array('url' => 'user/' . $user->id)) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::button('', array('class'=>'btn btn-danger ion-ios-trash buttonIndex', 'type'=>'submit'))}}
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert-success').hide();
        }, 5000);

        setTimeout(function () {
            $('.alert-danger').hide();
        }, 5000);
    </script>
@endsection