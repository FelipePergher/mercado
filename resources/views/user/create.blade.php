@extends('layouts.template')

@section('content')

<link href="{{ asset('styles/indexUsuario.css') }}" rel="stylesheet">

<div class="margemForm">
        <h2> <a href="{{route('user.index')}}"><span class="ion-arrow-left-c"></span></a> Cadastrar Usuário</h2>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-horizontal">
                        {{Form::model($users, array('route' => array('user.store'),'id' => 'crud-form', 'files' => 'true'))}}

                    <div class="form-group">
                        {{Form::label('nome', 'Nome:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('nome',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('nome'))
                            {{$errors->first('nome')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('sobrenome', 'Sobrenome:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('sobrenome',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('sobrenome'))
                            {{$errors->first('sobrenome')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('datanascimento', 'Data de nascimento:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::date('datanascimento',null,['class' => 'col-lg-8 form','autocomplete' => 'off'])}}
                        @if ($errors->has('dattanascimento'))
                            {{$errors->first('datanascimento')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('cpf', 'CPF:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('cpf',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('cpf'))
                            {{$errors->first('cpf')}}
                        @endif
                        
                    </div>

                    <div class="form-group">
                        {{Form::label('rg', 'RG:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('rg',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('rg'))
                            {{$errors->first('rg')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('telefone', 'Telefone:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('telefone',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('telefone'))
                            {{$errors->first('telefone')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('celular', 'Celular:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('celular',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('celular'))
                            {{$errors->first('celular')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('rua', 'Rua:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('rua',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('rua'))
                            {{$errors->first('rua')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('bairro', 'Bairro:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('bairro',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('bairro'))
                            {{$errors->first('bairro')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('complemento', 'Complemento:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('complemento',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('numero', 'Numero:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('numero',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('numero'))
                            {{$errors->first('numero')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('cep', 'CEP:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::text('cep',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('cep'))
                            {{$errors->first('cep')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('email', 'E-mail:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::email('email',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('email'))
                            {{$errors->first('email')}}
                        @endif
                    </div>
                
                    <div class="form-group">
                        {{Form::label('role', 'Função:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::select('role',array('gerente' => 'Gerente', 'vendedor' => 'Vendedor'),'vendedor',array('class' => 'tipo form col-lg-8'))}}
                    </div>

                    <div class="form-group">
                        {{Form::label('password', 'Senha:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::password('password',['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('password'))
                            {{$errors->first('pasawword')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::label('password_confirmation', 'Confirmação Senha:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::password('password_confirmation',['class' => 'col-lg-8 form','autocomplete' => 'off'])}}
                        @if ($errors->has('password_confirmation'))
                            {{$errors->first('password_confirmation')}}
                        @endif
                    </div>

                    <div class="form-group">
                        {{Form::reset('Limpar', array('class' => 'btn btn-danger limpar'))}}
                        {{Form::submit('Salvar', array('class' => 'btn btn-success salvar', 'id' => 'validar'))}}
                        {{Form::close()}}
                    </div>
                </div>
            </div>  
        </div>
@endsection


@section('script')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator->selector('#crud-form') !!}
@endsection