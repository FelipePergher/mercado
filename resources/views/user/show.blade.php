@extends('layouts.template')
@section('content')
<link href="{{ asset('styles/indexUsuario.css') }}" rel="stylesheet">

<div class="margem">
<h2> <a href="/user"><span class="ion-arrow-left-c"></span></a> Informações do Usuário</h2>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
                <table class="table table-bordered table-striped table-responsive">
                    <tr>
                        <th>Nome</th>
                        <td>{{$users->nome}}</td>
                    </tr>
                    <tr>
                        <th>Sobrenome</th>
                        <td>{{$users->sobrenome}}</td>
                    </tr>
                    <tr>
                        <th>CPF</th>
                        <td>{{$users->cpf}}</td>
                    </tr>
                    <tr>
                        <th>RG</th>
                        <td>{{$users->rg}}</td>
                    </tr>
                    <tr>
                        <th>Telefone</th>
                        <td>{{$users->telefone}}</td>
                    </tr>
                    <tr>
                        <th>Celular</th>
                        <td>{{$users->celular}}</td>
                    </tr>
                    <tr>
                        <th>Rua</th>
                        <td>{{$users->rua}}</td>
                    </tr>
                    <tr>
                        <th>Bairro</th>
                        <td>{{$users->bairro}}</td>
                    </tr>
                    <tr>
                        <th>Complemento</th>
                        <td>{{$users->complemento}}</td>
                    </tr>
                    <tr>
                        <th>Numero</th>
                        <td>{{$users->numero}}</td>
                    </tr>
                    <tr>
                        <th>CEP</th>
                        <td>{{$users->cep}}</td>
                    </tr>
                    <tr>
                        <th>Data de nascimento</th>
                        <td>{{$users->datanascimento}}</td>
                    </tr>
                    <tr>
                        <th>Função</th>
                        <td>{{$users->role}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection