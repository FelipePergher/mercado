@extends('layouts.template')
@section('content')


<link href="{{ asset('styles/productDetails.css') }}" rel="stylesheet"> 

<div class="margem">
  <h2 id="categoriaId"> {{$product->nomeProduto}}</h2>
  <div class="row">
    <div class = "col-md-5">
      <div class="panel panel-default painelImagem">
        <div class="panel-body">
          <img src="{{ asset('storage/media/products/'.$product->imagem) }}" class="center-block" height="100%" width="100%">
        </div>
      </div>
    </div>

    <div class = "col-md-7 texto"> 
        <div class="panel panel-default painelInformacoes">
          <div class="panel-body">

            <h4>Informações do produto: </h4>

              <div class="row">
                <div class="col-md-6 codigo">
                  <div class="row codigo">
                    {{'Código de barras: '.$product->codProduto}}
                  </div>

                  <div class="row peso">
                    {{'Peso: '.$product->peso}}
                  </div>
                </div>

                <div class="col-md-6">
                  @if ($product->glutem == 1)
                    <div class="row alergenicos ion-alert-circled">
                      Contém glútem
                    </div>
                  @endif

                  @if($product->glutem == 0 && ($product->categoria == "Alimentos" || $product->categoria == "Doces/Salgados" ||
                    $product->categoria == "Padaria" || $product->categoria == "Bebidas"))
                    <div class="row alergenicos ion-alert-circled">
                      Não contém glútem
                    </div>
                  @endif

                  @if ($product->lactose == "1")
                    <div class="row alergenicos ion-alert-circled">
                      Contém lactose
                    </div>
                  @endif

                  @if($product->lactose == 0 && ($product->categoria == "Alimentos" || $product->categoria == "Doces/Salgados" ||
                    $product->categoria == "Padaria" || $product->categoria == "Bebidas"))
                    <div class="row alergenicos ion-alert-circled">
                      Não contém lactose
                    </div>
                  @endif
                </div>
              </div>   

              <div class="row ingredientes">
                  {{'Ingredientes/composição: '.$product->composicao}}
              </div>
          </div>
        </div>
        <div class="panel panel-default painelDemaisInfo">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <h5>Valor Unitário</h5>
                  <h5>
                    @if($product->promocao == 0)
                      {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                    @elseif ($product->promocao == 1)
                      <div class="tachado">
                        {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                      </div>
                      {{'R$ '.number_format(($product->preco*(100-$product->descontoPromocao)/100), 2, ',', '.') }}
                    @endif
                  </h5>
                </div>
              <div class="col-md-2 centerVert">
                <input class='inputQuantidade cart' id="qtCompra" type='number' min='0' max="{{$product->qntEstoque}}" 
                    step='1'value="1" name='quantidade'>
              </div>
              <div class="col-md-2">
                @if ($product->qntEstoque <= 0)
                <button class="btn btn-success botao2 disabled">Produto Indisponível</button>
                @else
                  @if($product->promocao == 0)
                  <button class="btn btn-success cart botao1" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                    data-preco="{{$product->preco}}" data-quantidade="3" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                    data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
                  @elseif ($product->promocao == 1)
                  <button class="btn btn-success cart botao1" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                    data-preco="{{($product->preco*(100-$product->descontoPromocao)/100)}}" data-quantidade="3" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                    data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
                  @endif
                
                @endif
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class = "row">
    @if($product->categoria == "Alimentos" || $product->categoria == "Doces - Salgados" 
    || $product->categoria == "Padaria" ||$product->categoria == "Frios" ||$product->categoria == "Bebidas")
      <div class="col-md-6">
        <h5>Informação Nutricional: </h5>
        <table class="table table-striped table-bordered">
          <tr>
            <th>Item</th>
            <th>Quantidade por porção (Porção 50g)</th>
          </tr>
          <tr>
            <td>Valor Calórico</td>
            <td>{{$product->valorCalorico}}</td>
          </tr>
          <tr>
            <td>Carboidratos</td>
            <td>{{$product->carboidratos}}</td>
          </tr>
          <tr>
            <td>Proteínas</td>
            <td>{{$product->proteinas}}</td>
          </tr>
          <tr>
            <td>Gorduras Totais</td>
            <td>{{$product->gordurasT}}</td>
          </tr>
          <tr>
            <td>Gorduras Saturadas</td>
            <td>{{$product->gordurasS}}</td>
          </tr>
          <tr>
            <td>Colesterol</td>
            <td>{{$product->colesterol}}</td>
          </tr>
          <tr>
            <td>Fibra Alimentar</td>
            <td>{{$product->fibraAlimentar}}</td>
          </tr>
          <tr>
            <td>Cálcio</td>
            <td>{{$product->calcio}}</td>
          </tr>
          <tr>
            <td>Ferro</td>
            <td>{{$product->ferro}}</td>
          </tr>
          <tr>
            <td>Sódio</td>
            <td>{{$product->sodio}}</td>
          </tr>
        </table>
      </div>
     

      <div class="col-md-6">
        <h5>Produtos relacionados:</h5>
        <div class="row"> 
          @foreach($relacionados as $product)
          <div class="col-md-6">
            <a href="{{route('getProductsDetails',$product->id)}}">
                <div class="panel panel-default painelRelacionados">
                  <div class="panel-body">
                    <div class="row imagem">
                      <img src="{{ asset('storage/media/products/'.$product->imagem) }}" class="center-block" height="80" width="80">
                    </div>

                    <div class="row nomeRelacionados">
                        {{$product->nomeProduto}}
                    </div>

                    <div class="row precoRelacionados">
                      {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                      {{ Form::close() }}
                    </div>
            </a>

              @if ($product->qntEstoque <= 0)
                <div align="center">
                  <button class="btn btn-success botao2 disabled">Produto Indisponível</button>
              @else

              <div align="center">
                <button class="btn btn-success cart botao2" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                data-preco="{{$product->preco}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
              @endif
              </div>
              
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      
      @else
      <h5>Produtos relacionados:</h5>
        <div class="row"> 
          @foreach($relacionados as $product)
          <div class="col-md-3">
          <a href="{{route('getProductsDetails',$product->id)}}">
              <div class="panel panel-default painelRelacionados">
                <div class="panel-body">
                  
                  <div class="row imagem">
                    <img src="{{ asset('storage/media/products/'.$product->imagem) }}" class="center-block" height="80" width="80">
                  </div>

                  <div class="row nomeRelacionados">
                      {{$product->nomeProduto}}
                  </div>

                  <div class="row precoRelacionados">
                    {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                    {{ Form::close() }}
                  </div>
                </a>

                @if ($product->qntEstoque <= 0)
                  <div align="center">
                    <button class="btn btn-success botao2 disabled ">Produto Indisponível</button>
                @else
                  <div align="center">
                    <button class="btn btn-success cart botao2" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                      data-preco="{{$product->preco}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                      data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
                @endif
                </div>
                
              </div>
            </div>
          </div>
        @endforeach
        </div>
      </div>
      @endif
  </div>
</div>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('scripts/cart.js')}}"></script>
@endsection