@extends('layouts.template')

@section('content')

<link href="{{ asset('styles/sale.css') }}" rel="stylesheet">

@if (session('sucesso'))
    <div class="ion-checkmark-circled alert alert-success" id="success">
        {{ session('sucesso') }}
    </div>
@endif

<div class="margem">
<h3> Lista de compras </h3>
  <table class="table table-bordered table-responsive table-striped" >
      <tbody id="venda">
      </tbody>
  </table>
  @if ($errors->has('error'))
    <div class="ion-alert-circled alert alert-danger" id="error">
      {{$errors->first('error')}}
    </div>
  @endif

  <div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <span >Entrega apenas na cidade de Fraiburgo</span>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <span style="margin-left: 40px;" >Frete no valor de R$ 10,00 (Preço único)</span>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <span id="valorTotal"  class="pull-right"></span>
    </div>
  </div>
  <div class="row finalizandoVenda">  
    <div class="form-horizontal ">
      </br><hr/>
      <h4> Informações do cliente </h4>
      {{Form::model($user,array('route' => array('storeSale'),'id' => 'sale-form', 'method' => 'POST'))}}
      {{Form::text('products',null,['style' => 'display:none', 'id' => 'products'])}}
      
      <div class="form-group">
        {{Form::label('bairro', 'Bairro:',['class' => 'col-lg-3 control-label'])}}
        {{Form::text('bairro',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('bairro'))
          {{$errors->first('bairro')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('rua', 'Rua:',['class' => 'col-lg-3 control-label'])}}
        {{Form::text('rua',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('rua'))
          {{$errors->first('rua')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('numero', 'Número:',['class' => 'col-lg-3 control-label'])}}
        {{Form::text('numero',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('numero'))
          {{$errors->first('numero')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('complemento', 'Complemento:',['class' => 'col-lg-3 control-label'])}}
        {{Form::text('complemento',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('complemento'))
          {{$errors->first('complemento')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('dataentrega', 'Data:',['class' => 'col-lg-3 control-label'])}}
        {{Form::date('dataentrega',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('dataentrega'))
          {{$errors->first('dataentrega')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('horariominimo', 'Horario Inicio :',['class' => 'col-lg-3 control-label'])}}
        {{Form::time('horariominimo',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('horariominimo'))
          {{$errors->first('horariominimo')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('horariofinal', 'Horario Final :',['class' => 'col-lg-3 control-label'])}}
        {{Form::time('horariofinal',null,['class' => 'form col-lg-6'])}}
        @if ($errors->has('horariofinal'))
          {{$errors->first('horariofinal')}}
        @endif
      </div>

      <div class="form-group">
        {{Form::label('tipoPagamento', 'Pagamento:',['class' => 'col-lg-3 control-label'])}}
        {{Form::text('tipoPagamento',"Pagamento na entrega (Cartão ou Dinheiro)",['class' => 'form col-lg-6', 'readonly'])}}
        @if ($errors->has('horariofinal'))
          {{$errors->first('horariofinal')}}
        @endif
      </div>
    </div>
  </div>

  @if ($errors->has('prod'))
      <h4>{{$errors->first('prod')}}</h4>
  @endif
  <div class="row botoes">
    <a href="{{ route('getProductsbyName') }}" class="pull-left btn btn-success maisProdutos">Escolher mais produtos</a>
    @if (Auth::guest())
      <a href="{{ route('login') }}" class="btn btn-success pull-right finalizar"> Finalizar Compra</a>
    @else 
        <button type="button" id="finalizarVenda" class="btn btn-success pull-right finalizar">Finalizar Compra</button>
    @endif
    <div class="finalizandoVenda">
      {{Form::submit('Finalizar Compra', array('class' => 'btn btn-success finalizar pull-right'))}}
      {{Form::close()}} 
    </div>
  </div>
</div>
@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
  <script type="text/javascript" src="{{ asset('scripts/sale.js')}}"></script>
  {!! $validator->selector('#sale-form') !!}
@endsection