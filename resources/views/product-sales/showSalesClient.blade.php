@extends('layouts.template')
@section('content')

<link href="{{ asset('styles/showSalesClient.css') }}" rel="stylesheet">

@if (session('sucesso'))
    <div class="ion-checkmark-circled alert alert-success">
        {{ session('sucesso') }}
    </div>
@endif

@if (session('erro'))
<div class="ion-alert-circled alert alert-danger">
    {{ session('erro') }}
</div>
@endif

<div class="margem">
    <div class="row">
        <div class="col-md-4">
            <h4>Endereço da Entrega:</h4> 
            <p><b>Bairro:</b> {{$entrega->bairro}}</p>
            <p><b>Rua:</b> {{$entrega->rua}}</p>
            <p><b>Número:</b> {{$entrega->numero}}</p>
            <p><b>Complemento:</b> {{$entrega->complemento}}</p>
        </div>

        <div class="col-md-4">
            <h4>Informações da venda:</h4>  
            <p><b>Quantidade total:</b> {{$sale->qntTotal}}</p>
            <p><b>Quantidade de produtos:</b> {{$sale->qntProdutos}}</p>
            <p><b>Valor total:</b> R$ {{$sale->valorTotal}}</p>
            
            @if ($entrega->pagamentorealizado == 0)
                <p><b><span class="ion-close-circled"></span> Pagamento não realizado</b> </p>
            @else
                <p><b><span class="ion-checkmark-circled"></span> Pagamento realizado</b> </p>
            @endif

            @if ($entrega->entregarealizada == 0)
                <p><b><span class="ion-close-circled"></span> Entrega não realizada</b></p>
            @else
                <p><b><span class="ion-checkmark-circled"></span> Entrega realizada</b> </p>
            @endif
        </div>

        <div class="col-md-4">
            <h4>Informações da Entrega:</h4> 
            @if ($sale->dataentrega == null)
                <p><b>Data da entrega: </b>{{$entrega->dataentrega}}</p>
            @else 
            <p><b>Data da entrega: </b>{{date( 'd/m/Y' , strtotime($entrega->dataentrega))}}</p>
            @endif
            
            <p><b>Horário início da entrega: </b>{{$entrega->horarioInicio}}</p> 
            <p><b>Horário fim da entrega: </b>{{$entrega->horarioFim}}</p> 
        </div>
    </div>

  <table class="table table-striped table-bordered">
    <h3>Produtos da venda</h3>
    <thead>
      <tr>
        <th>Nome do produto</th>
        <th>Quantidade</th>
        <th>Valor unitário</th>
        <th>Valor total</th>
      </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product->nomeProdutoSale}}</td>
                <td>{{$product->qnt}}</td>
                <td>{{  'R$ '.number_format($product->preco, 2, ',', '.') }}  </td>
                <td>{{  'R$ '.number_format($product->precoTotalProduto   , 2, ',', '.')}}</td>
            </tr>
        @endforeach

    </tbody>
  </table>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert-success').hide();
        }, 5000);

        setTimeout(function () {
            $('.alert-danger').hide();
        }, 5000);
    </script>
@endsection