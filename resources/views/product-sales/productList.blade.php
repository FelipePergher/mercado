@extends('layouts.template')
@section('content')

<link href="{{ asset('styles/productList.css') }}" rel="stylesheet">

<h2 id="categoriaId"> <a href="/home"><span class="ion-arrow-left-c pull-left"></span></a> {{$categoria}}</h2>

<div class = "margem">
  <div class="row">
    <hr/>
    {{Form::open(array('route' => array('getProductsbyName'), 'method' => 'GET'))}}
      {{Form::label('nome', 'Digite o nome do produto:',['class' => 'col-lg-3 control-label'])}}
      {{Form::text('nome',"",['class' => 'col-lg-7', 'style' => 'border-radius:4px; width: 50%;'])}}
      {{Form::submit('Consultar', array('class' => 'btn btn-danger pull-right' ,'style' => 'margin-top:-5px; margin-right:10%;'))}}
    {{ Form::close() }}
    </br></br>
  </div>
  <div class="row painel">
    @foreach($products as $product)
    <div class = "col-md-3">
      <a href="{{route('getProductsDetails',$product->id)}}">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row imagem">
              <img src="{{ asset('storage/media/products/'.$product->imagem) }}" class="center-block" height="80" width="80">
            </div>
            <div class="row nomeProduto">
              {{$product->nomeProduto}}
              
            </div>
            <div class="row precoProduto">
            @if($product->promocao == 0)
              <br>
              {{'R$ '.number_format($product->preco, 2, ',', '.') }}
            @elseif ($product->promocao == 1)
              <div class="tachado">
                {{'R$ '.number_format($product->preco, 2, ',', '.') }}
              </div>
              {{'R$ '.number_format(($product->preco*(100-$product->descontoPromocao)/100), 2, ',', '.') }}
            @endif
              {{ Form::close() }}
            </div>
            </a>
            @if ($product->qntEstoque <= 0)
              <div align="center">
              <button class="btn btn-success disabled">Produto Indisponível</button>
            @else
              <div align="center">
              @if($product->promocao == 0)
                <button class="btn btn-success cart" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                data-preco="{{$product->preco}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
              @elseif ($product->promocao == 1)
                <button class="btn btn-success cart" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                data-preco="{{($product->preco*(100-$product->descontoPromocao)/100)}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
              @endif
            @endif
              </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
<div class="row paginator">
  {{ $products->links() }}
</div>

@endsection
@section('script')
  <script type="text/javascript" src="{{ asset('scripts/cart.js')}}"></script>
@endsection