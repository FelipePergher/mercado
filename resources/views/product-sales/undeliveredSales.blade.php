@extends('layouts.template')
@section('content')

<link href="{{ asset('styles/indexProduto.css') }}" rel="stylesheet">

<div class="margem">
  <h2 class="titulo">Vendas não entregues</h2>  
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Total de itens</th>
        <th>Valor total</th>
        <th>Data de compra</th>
        <th>Data de entrega</th>
        <th>Horário inicio</th>
        <th>Horário final</th>
        <th>Status da venda</th>
        <th>Detalhes</th>
        <th>Alterar data de entrega</th>
      </tr>
    </thead>
    <tbody>
        @foreach($sales as $sale)
            <tr>
                <td>{{$sale->qntTotal}}</td>
                <td>{{'R$ '.number_format($sale->valorTotal, 2, ',', '.') }}</td>
                <td>{{date( 'd/m/Y h:m', strtotime($sale->created_at))}}</td>
                @if ($sale->dataentrega == null)
                    <td>{{$sale->dataentrega}}</td>
                @else 
                    <td>{{date( 'd/m/Y' , strtotime($sale->dataentrega))}}</td>
                @endif
                <td>{{$sale->horarioinicio}}</td>
                <td>{{$sale->horariofim}}</td>
                <td>
                    @if($sale->entregarealizada == 1)
                        <div class="row ion-checkmark-circled">
                            Produto Entregue
                        </div>
                    @else
                        <div class="row ion-close-circled">
                            Produto não entregue
                        </div>
                    @endif
                   
                    @if($sale->pagamentorealizado == 1)
                        <div class="row ion-checkmark-circled">
                            Produto Pago
                        </div>

                    @else
                        <div class="row ion-close-circled">
                            Pagamento não realizado
                        </div>
                    @endif
                </td>
                <td><a href="{{route('showSalesClient',$sale->id)}}"><button type="button" class="btn btn-danger ion-information buttonIndex" ></button></a></td>
                <td>
                    <a data-toggle="modal" href='#modalEntrega-{{$sale->id}}' type="button" class="btn btn-danger ion-edit buttonIndex"></a>
                    <div class="modal fade" id="modalEntrega-{{$sale->id}}" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Alterar Data Entrega</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal ">
                                        {{Form::model($sale, array('route' => array('updateEntrega',$sale->id)))}}
                                        <div class="form-group">
                                            {{Form::label('dataentrega', 'Data de entrega:')}}
                                            {{Form::date('dataentrega')}}
                                            
                                        </div>
                                        <div class="form-group">
                                            {{Form::label('horarioInicio', 'Horario Inicial :')}}
                                            {{Form::time('horarioInicio',null)}}
                                        </div>
                                        <div class="form-group">
                                            {{Form::label('horariofinal', 'Horario Final :')}}
                                            {{Form::time('horariofinal',null)}}    
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    {{Form::submit('salvar', array('class' => 'btn btn-success'))}}
                                    {{ Form::close() }}    
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach 
    </tbody>
  </table>
</div>


@endsection

@section('script')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert-success').hide();
        }, 5000);

        setTimeout(function () {
            $('.alert-danger').hide();
        }, 5000);
    </script>
@endsection