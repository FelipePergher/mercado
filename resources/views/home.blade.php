@extends('layouts.template')
@section('content')

<link href="{{ asset('styles/home.css') }}" rel="stylesheet">

@if (session('erro'))
    <div class="ion-alert-circled alert alert-danger">
        {{ session('erro') }}
    </div>
@endif

<div id="carousel-id" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel-id" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
        <li data-target="#carousel-id" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img src="{{ asset('img/Banner1.png') }}" class="center-block" style="width: 100%">
        </div>
        <div class="item">
            <img src="{{ asset('img/Banner2.png') }}" class="center-block" style="width: 100%">
        </div>
        <div class="item">
            <img src="{{ asset('img/Banner3.png') }}" class="center-block" style="width: 100%">
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span id="carrouselLeft" class="ion-chevron-left"></span></a>
    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span id="carrouselRight" class="ion-chevron-right"></span></a>
</div>

<div class = "margem">
<h2 id="homeaId">Confira nossas promoções:</h2>
  <div class="row painel">
    @foreach($products as $product)
    <div class = "col-md-3">
      <a href="{{route('getProductsDetails',$product->id)}}">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row imagem">
              <img src="{{ asset('storage/media/products/'.$product->imagem) }}" class="center-block" height="80" width="80">
            </div>
            <div class="row nomeProduto">
              {{$product->nomeProduto}}
              
            </div>
            <div class="row precoProduto">
                @if($product->promocao == 0)
                    {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                @elseif ($product->promocao == 1)
                    <div class="tachado">
                        {{'R$ '.number_format($product->preco, 2, ',', '.') }}
                    </div>
                    {{'R$ '.number_format(($product->preco*(100-$product->descontoPromocao)/100), 2, ',', '.') }}
                @endif
              {{ Form::close() }}
            </div>
            </a>
            @if ($product->qntEstoque <= 0)
              <div align="center">
              <button class="btn btn-success disabled">Produto Indisponível</button>
            @else
              <div align="center">
                 @if($product->promocao == 0)
                    <button class="btn btn-success cart" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                    data-preco="{{$product->preco}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                    data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
                @elseif ($product->promocao == 1)
                    <button class="btn btn-success cart" align="center" id="cart-{{$product->id}}" data-valor="add" data-id="{{$product->id}}"
                    data-preco="{{($product->preco*(100-$product->descontoPromocao)/100)}}" data-image="{{$product->imagem}}" data-qntAtual="{{$product->qntEstoque}}"
                    data-nome="{{$product->nomeProduto}}">Adicionar ao carrinho</button>
                @endif
            @endif
              </div>
          </div>
        </div>
        
      </div>
    @endforeach
  </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);

        setTimeout(function () {
            $('.alert-danger').hide();
        }, 3000);
    </script>
    <script type="text/javascript" src="{{ asset('scripts/cart.js')}}"></script>
@endsection

