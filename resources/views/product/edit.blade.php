@extends('layouts.template')
@section('content')
<link href="{{ asset('styles/indexProduto.css') }}" rel="stylesheet">

<div class="margemForm">
        <h2> <a href="{{route ('product.index')}}"><span class="ion-arrow-left-c"></span></a> Edição de Produto</h2>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-horizontal">
                    {{ Form::model($product, array('route' => array('product.update', $product->id), 'method' => 'PUT' , 'id' => 'crud-form', 'files' => 'true'))}}

                    <div class="form-group">
                        {{Form::label('nomeProduto', 'Nome:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('nomeProduto',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('nomeProduto'))
                                {{$errors->first('nomeProduto')}}
                        @endif 
                    </div>

                    <div class="form-group">
                        {{Form::label('qntEstoque', 'Quantidade em estoque:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('qntEstoque',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('qntEstoque'))
                            {{$errors->first('qntEstoque')}}
                        @endif 
                    </div>

                    <div class="form-group">
                        {{Form::label('tipoProduto', 'Tipo Produto:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::label('tipoProduto', 'Não Perecível:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::radio('tipoProduto', 'Não Perecível', 'Não Perecível',['class' => 'col-lg-2 form']) }}
                        {{Form::label('tipoProduto', 'Perecível:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::radio('tipoProduto', 'Perecível', 'Perecível',['class' => 'col-lg-2 form']) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('preco', 'Preço:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('preco',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('preco'))
                            {{$errors->first('preco')}}
                        @endif 
                    </div>

                    <div class="form-group">
                        {{Form::label('codProduto', 'Código de Barras:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('codProduto',null,['class' => 'col-lg-8 form'])}}
                        @if ($errors->has('codProduto'))
                            {{$errors->first('codProduto')}}
                        @endif 
                    </div>

                    <div class="form-group">
                        {{Form::label('unidadeProduto', 'Unidade do Produto:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::label('unidadeProduto', 'Kg:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::radio('unidadeProduto', 'kg', 'kg',['class' => 'col-lg-2 form']) }}
                        {{Form::label('unidadeProduto', 'Und:',['class' => 'col-lg-2 control-label'])}}
                        {{Form::radio('unidadeProduto', 'und', 'und',['class' => 'col-lg-2 form']) }}
                    </div>
            
                    <div class="form-group">
                        {{Form::label('categoria', 'Categoria:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::select('categoria',$categorias,$product->categoria,array('class' => 'form col-lg-8','id'=>'selectCategoria'))}}
                    </div>

                    <div class="form-group">
                        {{Form::label('composicao', 'Ingredientes/Composição:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('composicao',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('peso', 'Peso/quantidade:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('peso',null,['class' => 'col-lg-8 form'])}}
                    </div>


                <div class="desabilitar" style='display:none;'>
                    <div class="form-group">
                        {{Form::label('glutem', 'Glútem:',['class' => 'col-lg-3 control-label'])}}
                        {{ Form::checkbox('glutem', 1, null) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('lactose', 'Lactose:',['class' => 'col-lg-3 control-label'])}}
                        {{ Form::checkbox('lactose',1, null) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('valorCalorico', 'Valor Calórico:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('valorCalorico',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('carboidratos', 'Carboidratos:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('carboidratos',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('proteinas', 'Proteínas:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('proteinas',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('gordurasT', 'Gorduras Totais:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('gordurasT',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('gordurasS', 'Gorduras Saturadas:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('gordurasS',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('colesterol', 'Colesterol:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('colesterol',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('fibraAliementar', 'Fibra Alimentar:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('fibraAlimentar',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('calcio', 'Cálcio: ',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('calcio',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('ferro', 'Ferro:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('ferro',null,['class' => 'col-lg-8 form'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('sodio', 'Sódio:',['class' => 'col-lg-3 control-label'])}}
                        {{Form::text('sodio',null,['class' => 'col-lg-8 form'])}}
                    </div>
                </div>

                <div class="form-group">
                    {{Form::reset('Limpar', array('class' => 'btn btn-danger limpar'))}}
                    {{Form::submit('Salvar', array('class' => 'btn btn-success salvar', 'id' => 'validar'))}}
                    {{Form::close()}}
                </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript" src="{{ asset('scripts/editProduct.js')}}"></script>
    {!! $validator->selector('#crud-form') !!}
@endsection