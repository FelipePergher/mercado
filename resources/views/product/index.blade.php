@extends('layouts.template')
@section('content')


<link href="{{ asset('styles/indexProduto.css') }}" rel="stylesheet">
<link href="{{ asset('styles/campoPesquisa.css') }}" rel="stylesheet">


@if (session('sucesso'))
    <div class="ion-checkmark-circled alert alert-success">
        {{ session('sucesso') }}
    </div>
@endif

@if (session('erro'))
<div class="ion-alert-circled alert alert-danger">
    {{ session('erro') }}
</div>
@endif

<div class="margem">
  <div class="form-group">
      <a href= "{{ route('product.create') }}" class ="btn btn-danger">
          Cadastrar Novo Produto
      </a>
  </div>
  <hr/>
  <div class="row pesquisa">
    {{Form::open(array('route' => array('product.index'), 'method' => 'GET'))}}
      {{Form::label('nome', 'Nome do produto:',['class' => 'col-lg-2 control-label', 'style' => 'margin-left:8%;'])}}
      {{Form::text('nome',"",['class' => 'col-lg-7', 'style' => 'border-radius:4px; width: 50%;'])}}
      {{Form::submit('Consultar', array('class' => 'btn btn-danger pull-left col-lg-1' ,'style' => 'margin-top:-5px; margin-left:2%'))}}
    {{ Form::close() }}
  </div>
  <hr/>
  <h2 class="titulo">Lista de Produtos</h2>  
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Imagem</th>
        @if(Auth::user()->role != 'vendedor')
        <th>Editar imagem</th>
        @endif
        <th>Quantidade</th>
        <th>Categoria</th>
        <th>Valor</th>
        <th>Promoção</th>
        @if(Auth::user()->role != 'vendedor')
          <th>Editar</th>
          <th>Remover</th>
        @endif
      </tr>
    </thead>
    <tbody>
      @foreach($products as $product)
        <tr>
          <td><a href="{{route('getProductsDetails',$product->id)}}">{{$product->nomeProduto}}<a></td>
          <td>
            <div class="pull-left">
              <a href="{{route('getProductsDetails',$product->id)}}">
                <img src="{{ asset('storage/media/products/'.$product->imagem) }}" height="80" width="80">
              </a>
            </div>
          </td>
          @if(Auth::user()->role != 'vendedor')
            <td>
              <a class="btn btn-success uploadImage" id="{{$product->id}}">Trocar imagem</a>
              <div style="display:none">
                {{Form::open(array('route' => array('updateImage'), 'files' => 'true'))}}
                {{Form::text('id',$product->id,['style' => 'display:none'])}}
                {{Form::file('image',['accept' => 'image/*', 'id' => 'file-'.$product->id, 'onchange' => 'javascript:this.form.submit();'])}}
                {{Form::close()}}
              </div>
            </td>
          @endif
          <td>{{$product->qntEstoque.' '.$product->unidadeProduto}}</td>
          <td>{{$product->categoria}}</td>
          <td> 
            @if($product->promocao == 0)
              <br>
              {{'R$ '.number_format($product->preco, 2, ',', '.') }}
            @elseif ($product->promocao == 1)
              <div class="tachado">
                {{'R$ '.number_format($product->preco, 2, ',', '.') }}
              </div>
              {{'R$ '.number_format(($product->preco*(100-$product->descontoPromocao)/100), 2, ',', '.') }}
            @endif
          </td>

          <td> 
            <a data-toggle="modal" href='#promocao-{{$product->id}}'><button type="button" class="btn btn-danger ion-cash buttonIndex" ></button></a>
            <div class="modal fade" id="promocao-{{$product->id}}" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Cadastrar Promoção</h4>
                  </div>
                  <div class="modal-body">
                      <div class="form-horizontal ">
                          {{Form::model($product, array('route' => array('updatePromotion',$product->id)))}}
                          <div class="form-group">
                            {{Form::label('promo', 'Promoção:')}}
                            {{Form::checkbox('promocao',1, null)}}
                          </div>
                          <div class="form-group">
                            {{Form::label('porcent', 'Porcentagem(%):')}}
                            {{Form::text('descontoPromocao',null)}}
                          </div>
                          <div class="form-group">
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      {{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
                      <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                      {{Form::close()}}
                  </div>
                </div>
              </div>
            </div>
          </td>
          @if(Auth::user()->role != 'vendedor')
            <td><a href= "{{route('editProduct',$product->id)}}"> <button type="button" class="btn btn-danger ion-edit buttonIndex"></button></a></td>
            <td>
              {{ Form::open(array('url' => 'product/' . $product->id, 'class' => 'pull-left')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger ion-ios-trash buttonIndex')) }}
              {{ Form::close() }}
            </td>
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert-success').hide();
        }, 5000);

        setTimeout(function () {
            $('.alert-danger').hide();
        }, 5000);

        $(".uploadImage").click(function(){
          $("#file-"+this.id).click();
        });
    </script>
@endsection