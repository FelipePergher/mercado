<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class acessoVendedor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          
        if (Auth::guest() || $request->user()->role == "vendedor") 
        {
            return redirect('/')->with('erro', 'Ops! É necessário ter permissão para acessar essa página.');
        }
        return $next($request);
    }
}
