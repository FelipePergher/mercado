<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductSale;
use App\EntregaSale;
use App\Sale;
use App\User;
use Illuminate\Support\Facades\Auth;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductSalesController extends Controller
{
    protected $validationRules=[
        'rua' => 'required|max:255',
        'bairro' => 'required|max:255',
        'complemento' => 'max:255',
        'numero' => 'required',
        'dataentrega' => 'date|after:today',
    ];

    protected $messages = [
        'rua.required' => 'Você deve inserir o nome da rua',
        'rua.max' => 'Você deve inserir no maximo 255 characters',
        'bairro.required' => 'Você deve inserir o nome do bairro',
        'bairro.max' => "Você deve inserir no maximo 255 characters",
        'complemento.max' => "Você deve inserir no maximo 255 characters",
        'numero.required' => "Você deve inserir o número da casa",
        'dataentrega.date' => "insira uma data no formato (02/02/1990)",
        'dataentrega.after' => "Insira uma data posterior a de hoje",
    ];
    

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['salesClient', 'showSalesClient', 'updateEntrega']]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getProductsbyCategory($categoria, Request $request)
     {
         $nome = $request->input("nome");
         $products = Product::where('nomeProduto','LIKE',"%$nome%")->where('categoria', '=' ,$categoria)->paginate(4);
         return view('product-sales/productList',['products'=> $products,'categoria'=>$categoria]);    
     }

     public function getProductsDetails($id)
     {
        $product = Product::find($id);
        $relacionados = DB::table('products')->where('categoria', '=' ,$product->categoria)->inRandomOrder()->take(4)->get();
        return view('product-sales/productDetails',['product'=> $product, 'relacionados' => $relacionados]);
     }

     public function getProductsbyName(Request $request)
     {
        $nome = $request->input("nome");
        $product = Product::where('nomeProduto','LIKE',"%$nome%")->paginate(4);
        return view('product-sales/products',['products'=> $product]);
     }

     public function unpaidSales(){
        $sales = DB::table('sales')->join('entrega_sales', 'sales.id', '=', 'saleId')->select('sales.*', 'entrega_sales.dataentrega',
        'entrega_sales.horarioinicio','entrega_sales.horariofim','entrega_sales.pagamentorealizado','entrega_sales.entregarealizada')
        ->where('pagamentorealizado','=', 0)->get();        
        return view('product-sales/unpaidSales',['sales' => $sales]);
     }

     public function undeliveredSales(){
        $sales = DB::table('sales')->join('entrega_sales', 'sales.id', '=', 'saleId')->select('sales.*', 'entrega_sales.dataentrega',
        'entrega_sales.horarioinicio','entrega_sales.horariofim','entrega_sales.pagamentorealizado','entrega_sales.entregarealizada')
        ->where('entregarealizada','=', 0)->get();        
        return view('product-sales/undeliveredSales',['sales' => $sales]);
     }

     public function sale(){
        $user = null;
        if (Auth::User() != null){
            $user = Auth::User();
        }
        $validator = JsValidator::make($this->validationRules, $this->messages);
        return view('product-sales/sale',['user' => $user, 'validator' => $validator]);
     }

     public function storeSale(Request $r){
        $data = $r->input("dataentrega");
        if ($data == null){
            $this->validationRules["dataentrega"] = "";
        }
        $v = Validator::make($r->all(), $this->validationRules, $this->messages);
        
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $products = $r->input("products");
        $produtos = json_decode($r->input('products'));
        if (empty($produtos)) {
            return redirect()->back()->withInput()->withErrors(['error' => 'Você precisa de ao menos um produto na venda']);
        }
        $valorTotal = 0;
        $quantidadeTotal = 0;
        $quantidadeProdutos = 0;

        foreach ($produtos as $produto){
            $produtoD = json_decode($produto);
            $id = $produtoD->{'id'};
            $quantidade = $produtoD->{'quantidade'};
            $prod = Product::find($id);
            if($prod->promocao == 0){
                $valAtual = $prod->preco;
            }else if($prod->promocao == 1){
                $valAtual = $prod->preco*(100-$prod->descontoPromocao)/100;
            }
            $val =  $valAtual * $quantidade;
            $valorTotal += $val;
            $quantidadeTotal += $quantidade;
            $quantidadeProdutos += 1;
            if ($prod->qntEstoque < $quantidade) {
                return redirect()->back()->withInput()->withErrors(["prod" => "Estoque do produto $prod->nomeProduto insuficiente! ($prod->qntEstoque em estoque)"]);
            }
        }

        $sale = new Sale();
        $sale->nomeCliente = Auth::user()->nome;
        $sale->clienteId = Auth::user()->id;
        $sale->qntTotal = $quantidadeTotal;
        $sale->qntProdutos = $quantidadeProdutos;
        $sale->valorTotal = $valorTotal;

        if ($sale->save()){
            foreach ($produtos as $produto){
                $productSale = new ProductSale();
                $produtoD = json_decode($produto);
                $id = $produtoD->{'id'};
                $quantidade = $produtoD->{'quantidade'};
                $prod = Product::find($id);
                $prod->qntEstoque -= $quantidade;
                $prod->update();

                $productSale->nomeProdutoSale = $prod->nomeProduto;

                $productSale->qnt = $quantidade;
                $productSale->preco = $prod->preco;

                $productSale->precoTotalProduto = $prod->preco * $quantidade + 10;
                $productSale->saleId = $sale->id; 
                $productSale->save();
            }

            $entrega = new EntregaSale();
            $entrega->bairro = $r->input("bairro");
            $entrega->rua = $r->input("rua");
            $entrega->numero = $r->input("numero");
            $entrega->complemento = $r->input("complemento");
            $entrega->dataentrega = $r->input("dataentrega");
            $entrega->horarioinicio = $r->input("horariominimo");
            $entrega->horariofim = $r->input("horariofinal");
            $entrega->pagamentoRealizado = false;
            $entrega->entregaRealizada = false;
            $entrega->tipoPagamento = $r->input("tipoPagamento");
            $entrega->saleId = $sale->id; 
            $entrega->save();
            return redirect()->route('sale')->with('sucesso', 'Venda registrada com sucesso!');;
        }
        return redirect()->back()->withInput();
     }

     public function updateEntrega(Request $r, $id){
        $entrega = EntregaSale::where('saleId','=',$id)->get();
        $entrega[0]->dataentrega = $r->input('dataentrega');
        $entrega[0]->horarioInicio = $r->input('horarioInicio');
        $entrega[0]->horarioFim = $r->input('horariofinal');
        if ($entrega[0]->update()){
            return redirect()->back()->with('sucesso', 'Data de entrega atualizada com sucesso!');
        }
        throw new \Exception("Houve um erro ao atualizar a Data de entrega");
     }

     public function salesClient($id)
     {
        if ((Auth::user()->role == "gerente" or Auth::user()->role == "vendedor") && $id == 0){
            $sales = DB::table('sales')->join('entrega_sales', 'sales.id', '=', 'saleId')->select('sales.*', 'entrega_sales.dataentrega',
            'entrega_sales.horarioinicio','entrega_sales.horariofim','entrega_sales.pagamentorealizado','entrega_sales.entregarealizada')->get();        
            return view('product-sales/salesClient',['sales' => $sales, 'id' => $id,'titulo'=>'Relatório de Vendas']);
        }

        if ($id != Auth::user()->id){
            return redirect()->back();
        }
        
        $sales = Sale::where('clienteId','=', $id)->join('entrega_sales', 'sales.id', '=', 'saleId')->select('sales.*', 'entrega_sales.dataentrega',
        'entrega_sales.horarioinicio','entrega_sales.horariofim','entrega_sales.pagamentorealizado','entrega_sales.entregarealizada')->get();
        return view('product-sales/salesClient',['sales' => $sales, 'id' => $id,'titulo'=>'Meus Pedidos']);
     }

     public function showSalesClient($id)
     {
        $sale = Sale::find($id);  

        if ($sale == null){
            return redirect()->back();
        }

        $products = ProductSale::where('saleId', '=', $id)->get();
        $entrega = EntregaSale::where('saleId','=',$id)->get();
        if ($entrega != null){
            $entrega = $entrega[0];
        }
        return view('product-sales/showSalesClient',['sale' => $sale,'products' => $products,'entrega' => $entrega]);
     }

     public function saveDelivery (Request $r){
        $id = $r->input('id');
        $entrega_sale= EntregaSale::where('saleId','=', $id)->first();
        $checkbox = $r->input('checkbox');
        
        if ($checkbox){
            $entrega_sale->entregarealizada = true;
        } else {
            $entrega_sale->entregarealizada =  false;
        }
        if ($entrega_sale->save()){
            return response()->json("sucesso");
        } else{
            return response()->json("error");
        }
     }

     public function savePayment(Request $r){
        $id = $r->input('id');
        $entrega_sale= EntregaSale::where('saleId','=', $id)->first();
        $checkbox = $r->input('checkbox'); 
        if ($checkbox == true){
            $entrega_sale->pagamentorealizado = 1;
        } 
        if ($checkbox == false){
            $entrega_sale->pagamentorealizado = 0;
        }

        if ($entrega_sale->save()){
            return response()->json("sucesso");
        } else{
            return response()->json("error");
        }
     }
}
