<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    use RegistersUsers;
    


    protected $validationRules=[
        'nome' => 'required|max:255',
        'sobrenome' => 'required|max:255',
        'email' => 'required|email|unique:users,email|max:255',
        'cpf' => 'required|min:11|unique:users,cpf',
        'rg' => 'required|min:7|unique:users,rg',
        'telefone' => 'required',
        'celular' => 'required',
        'rua' => 'required|max:255',
        'bairro' => 'required|max:255',
        'complemento' => 'max:255',
        'numero' => 'required',
        'cep' => 'required|min:8',
        'datanascimento' => 'required|date|before:today',
        'password' => 'required|min:8|confirmed|max:60',
    ];

    protected $messages = [
        'nome.required' => 'Você deve inserir um nome.',
        'nome.max' => 'Você pode inserir no máximo 255 caracteres.',
        'sobrenome.required' => 'Você deve inserir um sobrenome.',
        'sobrenome.max' => 'Você pode inserir no máximo 255 caracteres.',

        'email.required' => 'Você deve inserir um email',
        'email.email' => 'Você deve inserir um endereço email válido',
        'email.unique' => 'Email não disponivel, insira outro',
        'email.max' => 'Você pode inserir no máximo 255 caracteres.',

        'cpf.required' => 'Você deve inserir um cpf.',
        'cpf.min' => 'Você deve inserir no mínimo 11 dígitos.',
        'cpf.unique' => 'Cpf não disponivel, insira outro',
        'cpf.max' => 'Você pode inserir no máximo 20 digitos.',

        'rg.integer' => 'Você deve inserir somente numeros.',
        'rg.min' => 'Você deve inserir no mínimo 7 dígitos.',
        'rg.required' => 'Você deve inserir um rg.',
        'rg.unique' => 'Rg não disponivel, insira outro',
        'rg.max' => 'Você pode inserir no máximo 20 digitos.',

        'telefone.required' => 'Você deve inserir um telefone.',
        'telefone.integer' => 'Você deve somente numeros.',
        'telefone.max' => 'Você pode inserir no máximo 20 digitos.',

        'celular.required' => 'Você deve inserir um celular.',
        'celular.integer' => 'Você deve somente numeros.',
        'celular.max' => 'Você pode inserir no máximo 20 digitos.',

        'rua.required' => 'Você deve inserir uma rua.',
        'rua.max' => 'Você pode inserir no máximo 255 caracteres.',

        'bairro.required' => 'Você deve inserir um bairro.',
        'bairro.max' => 'Você pode inserir no máximo 255 caracteres.',

        'complemento.max' => 'Você pode inserir no máximo 255 caracteres.',

        'numero.required' => 'Você deve inserir um numero.',
        'numero.integer' => 'Você deve apenas numeros.',
        'numero.max' => 'Você pode inserir no máximo 20 digitos.',

        'cep.required' => 'Você deve inserir o cep.',
        'cep.min' => 'Você deve inserir no minimo 8 digitos.',

        'datanascimento.required' => 'Você deve inserir uma data de nascimento',
        'datanascimento.date' => 'Data Invalida',
        'datanascimento.before' => 'Insira uma data anterior a hoje',

        'password.required' => 'Você deve inserir uma senha',
        'password.min' => 'Você deve ser de no mínimo 8 dígitos',
        'password.max' => 'Você pode ser de no maximo 60 dígitos',
        'password.confirmed' => "Confirmação de senha deve ser igual a senha"
    ];

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function showRegistrationForm()
    {
        $validator = JsValidator::make($this->validationRules, $this->messages);
        $user = new User();
        return view ('auth/register', ['user' => $user, 'validator' => $validator]);
    }

    protected function register(Request $request)
    {
        $v = Validator::make($request->all(), $this->validationRules, $this->messages);
        
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $user = new User();
        $user->nome = $request->input('nome');
        $user->sobrenome = $request->input('sobrenome');
        $user->email= $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->cpf = $request->input('cpf');
        $user->rg = $request->input('rg');
        $user->telefone= $request->input('telefone');
        $user->celular= $request->input('celular');
        $user->rua= $request->input('rua');
        $user->bairro= $request->input('bairro');
        $user->numero= $request->input('numero');
        $user->complemento= $request->input('complemento');
        $user->cep= $request->input('cep');
        $user->datanascimento = $request->input('datanascimento');
        
        if($user->save()){
            $email = $request->get('email');
            $password = $request->get('password');

            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return redirect()->route('home');
            } else {
                throw new \Exception("E-mail ou senha inválidos");
            }
            //return redirect()->route('registersuccess');
        } else{
            throw new \Exception("Houve um erro ao cadastrar o usuário");
        }
        
    }
}
