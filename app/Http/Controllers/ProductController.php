<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use File;
use JsValidator;
use Illuminate\Support\Facades\Validator;
class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('acessoAdministrativo');
        $this->middleware('acessoVendedor',['except' => ['index']]);
        $this->middleware('auth');
    }

    protected $validationRules=[
        'nomeProduto' => 'required|max:255|unique:products,nomeProduto',
        'codProduto' => 'required|max:9999999999999999999|unique:products,codProduto',
        'qntEstoque' => 'required|integer|max:999999999999999999',
        'image' => 'required|image',
        'preco' => 'required|max:99999999',
        'categoria' => 'required',
        'composicao' => 'required|max:255|',
        'peso'=>'required',
        'valorCalorico'=>'required',
        'carboidratos'=>'required',
        'proteinas'=>'required',
        'gordurasT'=>'required',
        'gordurasS'=>'required',
        'colesterol'=>'required',
        'fibraAlimentar'=>'required',
        'calcio'=>'required',
        'ferro'=>'required',
        'sodio'=>'required',
    ];

    protected $messages = [
        'nomeProduto.required' => 'Você deve inserir um nome.',
        'nomeProduto.max' => 'Você deve inserir no máximo 255 caracteres',
        'nomeProduto.unique' => 'Nome não disponivel, insira outro',

        'qntEstoque.required' => 'Você deve inserir a quantidade em estoque.',
        'qntEstoque.integer' => 'Quantidade deve ser inteira.',
        'qntEstoque.max' => 'Você deve inserir no máximo 20 digitos',

        'image.required' => 'Você deve inserir uma imagem.',
        'image.Image' => 'Você deve inserir uma imagem.',

        'preco.required' => 'Você deve inserir o valor do produto.',
        'preco.max' => 'Você deve inserir no máximo 8 digitos',

        'codProduto.required' => 'Você deve inserir o código de barras do produto.',
        'codProduto.integer' => 'O código de barras é somente numeros.',
        'codProduto.unique' => 'Codigo de Barra ja cadastrado, insira outro',
        'codProduto.max' => 'Você deve inserir 13 dígitos',

        'categoria.required' => 'Você deve informar a categoria do produto.',

        'composicao.required' => 'Você deve informar a composição do produto',
        'composicao.max'=>'Você deve inserir no máximo 255 caracteres',
        'peso.required' => 'Você deve informar o peso do produto',
        'valorCalorico.required' => 'Você deve informar o valor calórico do produto',
        'carboidratos.required' => 'Você deve informar um valor para carboidratos do produto',
        'proteinas.required' => 'Você deve informar as proteínas do produto',
        'gordurasT.required' => 'Você deve informar as gorduras totais do produto',
        'gordurasS.required' => 'Você deve informar as gorduras saturadas do produto',
        'colesterol.required' => 'Você deve informar o valor de colesterol do produto',
        'fibraAlimentar.required' => 'Você deve informar a fibra alimentar do produto',
        'calcio.required' => 'Você deve informar o cálcio do produto',
        'ferro.required' => 'Você deve informar o valor de ferro do produto',
        'sodio.required' => 'Você deve informar o valor do sódio do produto',
    ];

    protected $categorias = [
        "Alimentos" => "Alimentos",
        "Doces - Salgados" => "Doces - Salgados",
        "Frutas - Verduras" => "Frutas - Verduras",
        "Higiene - Limpeza" => "Higiene - Limpeza",
        "Padaria" => "Padaria",
        "Frios" => "Frios",
        "Bebidas" => "Bebidas",
        "Itens diversos" => "Itens diversos",
    ];

    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nome = $request->input("nome");
        $products = Product::where('nomeProduto','LIKE',"%$nome%")->get();        
        return view('product/index',['products'=> $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validationRules, $this->messages);
        $product = new Product();
        return view('product/create',['product' => $product, 'categorias' => $this->categorias, 'validator' => $validator]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = $request->input('categoria');
        if($categoria == "Higiene - Limpeza" or $categoria == "Frutas - Verduras" or $categoria == "Itens diversos")
        {
            $this->validationRules["valorCalorico"] = "";
            $this->validationRules["gordurasT"] = "";
            $this->validationRules["proteinas"] = "";
            $this->validationRules["carboidratos"] = "";
            $this->validationRules["gordurasS"] = "";
            $this->validationRules["colesterol"] = "";
            $this->validationRules["calcio"] = "";
            $this->validationRules["fibraAlimentar"] = "";
            $this->validationRules["ferro"] = "";
            $this->validationRules["sodio"] = "";
        }
        $v = Validator::make($request->all(), $this->validationRules, $this->messages);
        
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        
        $fileName = null;

        if ($request->hasFile('image')){
            $file = $request->file('image'); 
            $fileName =  time().'.'.$file-> getClientOriginalExtension();
            $fileName = str_replace(" ","",$fileName);
        }

        $product = new Product();
        $product->nomeProduto = $request-> input('nomeProduto');
        $product->qntEstoque = $request->input('qntEstoque');
        $product->imagem = $fileName;
        $product->categoria = $categoria;
        $product->tipoProduto = $request->input('tipoProduto');
        $product->preco = $request->input('preco');
        $product->codProduto = $request->input('codProduto');
        $product->unidadeProduto =  $request->input('unidadeProduto');
        $product->composicao =  $request->input('composicao');
        $product->peso =  $request->input('peso');
        $product->glutem =  $request->input('glutem');
        $product->lactose =  $request->input('lactose');
        $product->valorCalorico =  $request->input('valorCalorico');
        $product->carboidratos =  $request->input('carboidratos');
        $product->proteinas =  $request->input('proteinas');
        $product->gordurasT =  $request->input('gordurasT');
        $product->gordurasS =  $request->input('gordurasS');
        $product->colesterol =  $request->input('colesterol');
        $product->fibraAlimentar =  $request->input('fibraAlimentar');
        $product->calcio =  $request->input('calcio');
        $product->ferro =  $request->input('ferro');
        $product->sodio =  $request->input('sodio');
        $product->promocao = 0;
        $product->descontoPromocao = 0;
        
        if($product->save()){
            $destinationPath = 'storage/media/products';
            if ($fileName != null){
                $file->move($destinationPath,$fileName);    
            }
            return redirect()->route('product.index')->with('sucesso', 'Produto cadastrado com sucesso!');;
        } else{
            throw new \Exception("Houve um erro ao cadastrar o produto");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        $validacaoNome = $this->validationRules["nomeProduto"];
        $validacaoNome = "$validacaoNome,$product->id";
        $this->validationRules["nomeProduto"] = $validacaoNome;

        $validacaoCodigo = $this->validationRules["codProduto"];
        $validacaoCodigo = "$validacaoCodigo,$product->id";
        $this->validationRules["codProduto"] = $validacaoCodigo;

        $this->validationRules["image"] = "";
        $validator = JsValidator::make($this->validationRules, $this->messages);
        
        
        return view('product.edit',['product' => $product, 'categorias' => $this->categorias, 'validator' => $validator]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $validacaoNome = $this->validationRules["nomeProduto"];
        $validacaoNome = "$validacaoNome,$product->id";
        $this->validationRules["nomeProduto"] = $validacaoNome;

        $validacaoCodigo = $this->validationRules["codProduto"];
        $validacaoCodigo = "$validacaoCodigo,$product->id";
        $this->validationRules["codProduto"] = $validacaoCodigo;

        $this->validationRules["image"] = "";

        $categoria = $request->input('categoria');
        if($categoria == "Higiene - Limpeza" or $categoria == "Frutas - Verduras" or $categoria == "Itens diversos")
        {
            $this->validationRules["valorCalorico"] = "";
            $this->validationRules["gordurasT"] = "";
            $this->validationRules["proteinas"] = "";
            $this->validationRules["carboidratos"] = "";
            $this->validationRules["gordurasS"] = "";
            $this->validationRules["colesterol"] = "";
            $this->validationRules["calcio"] = "";
            $this->validationRules["fibraAlimentar"] = "";
            $this->validationRules["ferro"] = "";
            $this->validationRules["sodio"] = "";
        }

        $v = Validator::make($request->all(), $this->validationRules, $this->messages);
        
        if ($v->fails())
        {
            return $v->errors();
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $product->nomeProduto = $request->input('nomeProduto');
        $product->qntEstoque = $request->input('qntEstoque');
        $product->categoria = $request->input('categoria');
        $product->tipoProduto = $request->input('tipoProduto');
        $product->preco = $request->input('preco');
        $product->codProduto = $request->input('codProduto');
        $product->unidadeProduto =  $request->input('unidadeProduto');
        $product->composicao =  $request->input('composicao');
        $product->peso =  $request->input('peso');
        $product->glutem =  $request->input('glutem');
        $product->lactose =  $request->input('lactose');
        $product->valorCalorico =  $request->input('valorCalorico');
        $product->carboidratos =  $request->input('carboidratos');
        $product->proteinas =  $request->input('proteinas');
        $product->gordurasT =  $request->input('gordurasT');
        $product->gordurasS =  $request->input('gordurasS');
        $product->colesterol =  $request->input('colesterol');
        $product->fibraAlimentar =  $request->input('fibraAlimentar');
        $product->calcio =  $request->input('calcio');
        $product->ferro =  $request->input('ferro');
        $product->sodio =  $request->input('sodio'); 
        
        if($product->update()){
            return redirect()->route('product.index')->with('sucesso', 'Produto atualizado com sucesso!');
        } else{
            throw new \Exception("Houve um erro ao atualizar o produto");
        }
    }

    public function updatePromotion(Request $request, $id)
    {
       
        $product = Product::find($id);
        $product->promocao = $request->input('promocao');
        $product->descontoPromocao = $request->input('descontoPromocao');
        if($product->update()){
            return redirect()->back()->with('sucesso', 'Promoção cadastrada com sucesso!');
        } else{
            throw new \Exception("Houve um erro ao cadastrar promoção");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        File::delete("storage/media/products/".$product->imagem);
        $product -> delete();

        return redirect () ->route ('product.index')->with('sucesso', 'Produto deletado com sucesso!');;   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateImage(Request $r){
        $id = $r->id;
        $product = Product::find($id);
        $file = $r->file('image'); 
        File::delete("storage/media/products/".$product->imagem);
        $destinationPath = 'storage/media/products';
        $file->move($destinationPath,$product->imagem);    
        return redirect()->route('product.index');
    }
}
