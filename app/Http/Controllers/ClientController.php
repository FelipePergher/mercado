<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('acessoAdministrativo', ['only' => ['index', 'show']]);
        $this->middleware('acessoVendedor', ['only' => ['index', 'show']]);
        $this->middleware('auth');
    }

    protected $validationRules=[
        'nome' => 'required|max:255',
        'sobrenome' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users,email',
        'cpf' => 'required|min:11|unique:users,cpf',
        'rg' => 'required|min:7|unique:users,rg',
        'telefone' => 'required',
        'celular' => 'required',
        'rua' => 'required|max:255',
        'bairro' => 'required|max:255',
        'complemento' => 'max:255',
        'numero' => 'required',
        'cep' => 'required|min:8',
        'datanascimento' => 'required|date|before:today',
        'password' => 'required|min:8|confirmed|max:60',
    ];

    protected $messages = [
        'nome.required' => 'Você deve inserir um nome.',
        'nome.max' => 'Você pode inserir no máximo 255 caracteres.',
        'sobrenome.required' => 'Você deve inserir um sobrenome.',
        'sobrenome.max' => 'Você pode inserir no máximo 255 caracteres.',

        'email.required' => 'Você deve inserir um email',
        'email.email' => 'Você deve inserir um endereço email válido',
        'email.unique' => 'Email não disponivel, insira outro',
        'email.max' => 'Você pode inserir no máximo 255 caracteres.',

        'cpf.required' => 'Você deve inserir um cpf.',
        'cpf.min' => 'Você deve inserir no mínimo 11 dígitos.',
        'cpf.unique' => 'Cpf não disponivel, insira outro',
        'cpf.max' => 'Você pode inserir no máximo 20 digitos.',
        'rg.min' => 'Você deve inserir no mínimo 7 dígitos.',
        'rg.required' => 'Você deve inserir um rg.',
        'rg.unique' => 'Rg não disponivel, insira outro',

        'telefone.required' => 'Você deve inserir um telefone.',
        'telefone.integer' => 'Você deve somente numeros.',
        'telefone.max' => 'Você pode inserir no máximo 20 digitos.',

        'celular.required' => 'Você deve inserir um celular.',
        'celular.integer' => 'Você deve somente numeros.',
        'celular.max' => 'Você pode inserir no máximo 20 digitos.',

        'rua.required' => 'Você deve inserir uma rua.',
        'rua.max' => 'Você pode inserir no máximo 255 caracteres.',

        'bairro.required' => 'Você deve inserir um bairro.',
        'bairro.max' => 'Você pode inserir no máximo 255 caracteres.',

        'complemento.max' => 'Você pode inserir no máximo 255 caracteres.',

        'numero.required' => 'Você deve inserir um numero.',
        'numero.integer' => 'Você deve apenas numeros.',
        'numero.max' => 'Você pode inserir no máximo 20 digitos.',

        'cep.required' => 'Você deve inserir o cep.',
        'cep.min' => 'Você deve inserir no minimo 8 digitos.',

        'datanascimento.required' => 'Você deve inserir uma data de nascimento',
        'datanascimento.date' => 'Data Invalida',
        'datanascimento.before' => 'Insira uma data anterior a hoje',

        'password.required' => 'Você deve inserir uma senha',
        'password.min' => 'Você deve ser de no mínimo 8 dígitos',
        'password.max' => 'Você pode ser de no maximo 60 dígitos',
        'password.confirmed' => "Confirmação de senha deve ser igual a senha"
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nome = $request->input("nome");
        $users = DB::table('users')->where('role', '=' ,'cliente')->where('nome','LIKE',"%$nome%")->get();
        return view('client/index', ['users'=> $users]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        $data = $users->datanascimento;
        $users->datanascimento = date("d-m-Y", strtotime($data));
        return view('client.show',['users' => $users]);
    }

    public function showClient($id)
    {
        $users = User::find($id);
        $data = $users->datanascimento;
        $users->datanascimento = date("d-m-Y", strtotime($data));
        return view('client.showClient',['users' => $users]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id == 1){
            return redirect()->route('home');
        }
        if ($id != Auth::user()->id && (Auth::user()->role == "vendedor" or Auth::user()->role == "cliente")){
            return redirect()->route('home');
        }

        $users = User::find($id);
        
        $validacaoEmail = $this->validationRules["email"];
        $validacaoEmail = "$validacaoEmail,$users->id";
        $this->validationRules["email"] = $validacaoEmail;

        $validacaoCpf = $this->validationRules["cpf"];
        $validacaoCpf = "$validacaoCpf,$users->id";
        $this->validationRules["cpf"] = $validacaoCpf;

        $validacaoRg = $this->validationRules["rg"];
        $validacaoRg = "$validacaoRg,$users->id";
        $this->validationRules["rg"] = $validacaoRg;

        $validator = JsValidator::make($this->validationRules, $this->messages);
        return view('client.edit',['users' => $users,'validator'=> $validator]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id == 1){
            return redirect()->route('home')->with('erro', 'Permissão negada!');
        }
        if ($id != Auth::user()->id && (Auth::user()->role == "vendedor" or Auth::user()->role == "cliente")){
            return redirect()->route('home')->with('erro', 'Permissão negada!');
        }

        $users = User::find($id);
        
        $validacaoEmail = $this->validationRules["email"];
        $validacaoEmail = "$validacaoEmail,$users->id";
        $this->validationRules["email"] = $validacaoEmail;

        $validacaoCpf = $this->validationRules["cpf"];
        $validacaoCpf = "$validacaoCpf,$users->id";
        $this->validationRules["cpf"] = $validacaoCpf;

        $validacaoRg = $this->validationRules["rg"];
        $validacaoRg = "$validacaoRg,$users->id";
        $this->validationRules["rg"] = $validacaoRg;

        if ($request->input('password') == null){
            $this->validationRules["password"] = "";
        } 

        $v = Validator::make($request->all(), $this->validationRules, $this->messages);
        
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $users->nome = $request->input('nome');
        $users->sobrenome = $request->input('sobrenome');
        $users->email = $request->input('email');
        $users->cpf = $request->input('cpf');
        $users->rg = $request->input('rg');
        $users->telefone = $request->input('telefone');
        $users->celular = $request->input('celular');
        $users->rua = $request->input('rua');
        $users->bairro = $request->input('bairro');
        $users->complemento = $request->input('complemento');
        $users->numero = $request->input('numero');
        $users->cep = $request->input('cep');
        $users->datanascimento = $request->input('datanascimento');
        
        if ($request->input('password') != null){
            $users->password = $request->input('password');
        }
 
         if($users->save()){
            return view('client.showClient',['users' => $users]);
         } else{
            return view('client.showClient',['users' => $users]);
         }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        
        if($users->delete()){
            return redirect()->route('client.index');
        } else{
            throw new \Exception("Houve um erro ao deletar cliente");
        }
    }
}
