$('.valueDelivery').change(function(){
    $.ajax({
        type: 'POST',
        url: "/salesClient/saveDelivery",
        data: {'_token': $('meta[name="csrf-token"]').attr('content'), id: this.id, checkbox: this.checked ? 1 : 0},
        dataType: 'JSON',
        success: function (data) {
            $.notify({
            	message: "Status da entrega alterado com sucesso!"
            },{
                type: 'success',
                timer: 4000
            }); 
        },
        error: function (data) {
            $.notify({
            	message: "Status da entrega não pode ser alterado!"
            },{
                type: 'danger',
                timer: 4000
            });
        }
    });
});

$('.valuePayment').change(function(){
    $.ajax({
        type: 'POST',
        url: "/salesClient/savePayment",
        data: {'_token': $('meta[name="csrf-token"]').attr('content'),id: this.id,checkbox: this.checked ? 1 : 0},
        dataType: 'JSON',
        success: function (data) {
            $.notify({
            	message: "Status do pagamento alterado com sucesso!"
            },{
                type: 'success',
                timer: 4000
            });     
        },
        error: function (data) {
            $.notify({
            	message: "Status do entrega não pode ser alterado!"
            },{
                type: 'danger',
                timer: 4000
            });
        }
    }); 
});