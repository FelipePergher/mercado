function timeEnable(botao){
    setTimeout(function() {
        $(botao).removeClass("disabled");
    }, 1000);
}

$("input[type=submit]").click(function () { 
    $(this).addClass("disabled");
    timeEnable(this);
});

if (localStorage.getItem("produtos") != null){
    $(".ion-ios-cart").text( " " + JSON.parse(localStorage.getItem("produtos")).length);    
}

$("form").attr('autocomplete', 'false');
$("input, :input").attr('autocomplete', 'false');

$(document).ready(function(){
  $(document).on( 'focus', ':input', function(){
    $(this).attr( 'autocomplete', 'off' );
    });
});

$(document).on( 'focus', ':input', function(){
    $(this).attr( 'autocomplete', 'off' );
});

type = ['','info','success','warning','danger'];

demo = {
	showNotification: function(from, align){
    	color = Math.floor((Math.random() * 4) + 1);

    	$.notify({
        	icon: "pe-7s-gift",
        	message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
	}
}