
$(document).ready(function($){
    $('#cpf').mask('999.999.999-99');      
});

$(document).ready(function($){
    $('#telefone').mask('(99)9999-9999');
});

$(document).ready(function($){
    $('#celular').mask('(99)99999-9999');
});

$(document).ready(function($){
    $('#cep').mask('99999-999');
});

$(document).ready(function($){
    $('#rg').mask('9.999.999');
}); 
$(document).ready(function($){
    $('.dataNascimento').mask('99/99/9999');
});

$(document).ready(function(){
    $('#preco').maskMoney();
})

$(document).ready(function(){
    $('#codProduto').mask('9999999999999');
})

$("#descontoPromocao").mask('##0,00%', {reverse: true});
