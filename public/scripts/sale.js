function listar(produtos){
    var tabela = $('#venda');
    var i = 0;
    var totalVenda = 10;

    tabela.empty();
    if (produtos.length > 0){
        tabela.append(
            "<tr>" +
            "<th>Produto</th>" +
            "<th>Valor Unitario</th>" +
            "<th>Quantidade</th>" +
            "<th>Valor total</th>" +
            "<th>Excluir</th>" +
            "</tr>"
            );
    }

    Number.prototype.format = function(n, x) {
        let re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\,' : '$') + ')';
        let num = this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        return num.replace('.',',');
    };
    
    jQuery.each(produtos, function(){
        var prod = JSON.parse(this);
        totalVenda += prod.valortotal;
        tabela.append(
            "<tr><td style='text-align:left' ><a href='/productsalesdetails/"+ prod.id + "'><img src='storage/media/products/"
            + prod.image+"' height='80' width='80'>   " + prod.nome + "</a>" +
            "</td><td> R$ " + prod.preco.format(2) +
            "</td><td><input class='inputQuantidade' id="+ prod.id +" type='number' min='0' max='" + prod.qntatual + 
            "' step='1' data-indice='" + i + 
            "' value='" + prod.quantidade + "' name='quantidade'>" +
            "</td><td> <span id='span-" + prod.id +"'> R$ " + prod.valortotal.format(2) + "</span>" +
            "</td><td> <span class='btn btn-danger ion-ios-trash remover' data-indice='" + i + "'></span>" +
            "</td></tr>");
            i++;
    });

    if (localStorage.getItem("produtos") != null){
        $(".ion-ios-cart").text( " " + JSON.parse(localStorage.getItem("produtos")).length);    
    }

    $(".inputQuantidade").change(function () {
        var produtos = localStorage.getItem("produtos");
        produtos = JSON.parse(produtos); 
        if(produtos == null) produtos = [];

        var quantidade = $(this).val();
        var indice = $(this).data("indice");
        var prod = JSON.parse(produtos[indice]);
        totalVenda -= prod.valortotal;
        if (quantidade > prod.qntatual){            
            $(this).val(prod.qntatual);
            return;
        }

        produtos[indice] = JSON.stringify({
			id   : prod.id,
            nome     : prod.nome,
            quantidade : quantidade,
            preco : prod.preco,
            image : prod.image,
            valortotal: quantidade * prod.preco,
            qntatual: prod.qntatual,
        });

        var prod = JSON.parse(produtos[indice]);
        $("#span-" + this.id).text("R$ " + prod.valortotal.format(2));
        totalVenda += prod.valortotal;
        $("#products").val(JSON.stringify(produtos));
        localStorage.setItem("produtos", JSON.stringify(produtos));   
        $("#valorTotal").html("Valor total R$ " + totalVenda.format(2));
    });

    $(".remover").click(function () { 
        produtos.splice($(this).data("indice"), 1);
        localStorage.setItem("produtos", JSON.stringify(produtos));   
        listar(produtos);
    });
    $("#products").val(JSON.stringify(produtos));
    $("#valorTotal").html("Valor total R$ " + totalVenda.format(2));
}

$(function () {
    var produtos = localStorage.getItem("produtos");
    produtos = JSON.parse(produtos); 
    if(produtos == null) produtos = [];
    
    if ($('#success').length != 0){
        produtos = [];
        localStorage.setItem("produtos", JSON.stringify(produtos));   
        listar(produtos);
        setTimeout(function () {
            $('#teste').hide();
        }, 5000);
    }

    listar(produtos);    
    $("#finalizarVenda").click(function () { 
        if (produtos.length === 0){
            alert("Você deve adicionar pelo menos um produto");
        } else{
            $(".finalizandoVenda").show();     
            $("#finalizarVenda").hide();
        }
    });

    setTimeout(function () {
        $('#error').hide();
    }, 5000);

    
});
