$(".cart").click(function () { 
    var produtos = localStorage.getItem("produtos");
    var valida = true;
    produtos = JSON.parse(produtos); 
    if(produtos == null) produtos = [];
    
    if ($(this).data("valor") === "add"){
        $(this).text("Remover do Carrinho");
        $(this).data("valor","remove");
        $(this).addClass("btn-danger").removeClass("btn-primary");

        //aqui  vou pegar o valor do input e jogar na variavel quantidade
        var qtnProduto = $('#qtCompra').val();
        if (qtnProduto === undefined){
            qtnProduto = 1;
        }

        var produto = JSON.stringify({
            id   : $(this).data("id"),
            nome     : $(this).data("nome"),
            quantidade : qtnProduto,
            preco : $(this).data("preco"),
            image : $(this).data("image"),
            valortotal : $(this).data("preco") * qtnProduto,
            qntatual : $(this).data("qntatual") ,
        });

        for (var i = 0; i < produtos.length; i++){
            var prod = JSON.parse(produtos[i]);
            if (produto.id === prod.id) {
                valida = false;
                break;
            }
        }
        if (valida == true){
            produtos.push(produto);
            localStorage.setItem("produtos", JSON.stringify(produtos));
        }
    }
    else if($(this).data("valor") === "remove"){
        $(this).text("Adicionar ao Carrinho");
        $(this).data("valor","add");
        $(this).removeClass("btn-danger").addClass("btn-primary");
        var id = $(this).data("id");
        var produtos = localStorage.getItem("produtos");
        produtos = JSON.parse(produtos); 
        if(produtos == null) produtos = [];
        
        for (var i = 0; i <= produtos.length; i++){
            var prod = JSON.parse(produtos[i]);
            if (id === prod.id) {
                produtos.splice(i,1);
                localStorage.setItem("produtos", JSON.stringify(produtos));
                break;;
            }
        }
    }

    if (localStorage.getItem("produtos") != null){
        $(".ion-ios-cart").text( " " + JSON.parse(localStorage.getItem("produtos")).length);    
    }
});

$(document).ready(function () {
    var produtos = localStorage.getItem("produtos");
    produtos = JSON.parse(produtos); 
    if(produtos == null) produtos = [];
    var products = $(".cart");
    
    if (produtos.length > 0){
        for (var i = 0; i < products.length; i++){
            var product = products[i];
            var id = $(product).data("id");

            for (var j = 0; j < produtos.length; j++){
                var prod = JSON.parse(produtos[j]);
                if (id === prod.id) {
                    $(product).text("Remover do Carinho");
                    $(product).data("valor","remove");
                    $(product).addClass("btn-danger").removeClass("btn-primary");
                    break;;
                }
            }
        }
    }

    
});