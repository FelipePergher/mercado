<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableProductSalesandSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeCliente');
            $table->integer('qntTotal');
            $table->integer('qntProdutos');
            $table->float('valorTotal', 8, 2);

            $table->integer('clienteId')->unsigned();
            $table->timestamps();
            $table->foreign('clienteId')->references('id')->on('users');
        });

        Schema::create('product_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeProdutoSale');
            $table->integer('qnt');
            $table->float('preco', 8, 2);
            $table->float('precoTotalProduto', 8, 2);

            $table->integer('saleId')->unsigned();
            $table->timestamps();        
            $table->foreign('saleId')->references('id')->on('sales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productSales');
        Schema::dropIfExists('sales');
    }
}
