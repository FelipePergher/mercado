<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('cpf')->unique();
            $table->string('rg')->unique();
            $table->string('telefone');
            $table->string('celular')->nullable();
            $table->string('rua');
            $table->string('bairro');
            $table->string('complemento')->nullable();
            $table->string('numero');
            $table->string('cep');
            $table->date('datanascimento');
            $table->enum('role',['cliente', 'gerente','vendedor'])->default('cliente');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
