<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntregaSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrega_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bairro');
            $table->string('rua');
            $table->integer('numero');
            $table->string('complemento')->nullable();
            $table->date('dataentrega')->nullable();

            $table->time('horarioInicio')->nullable();
            $table->time('horarioFim')->nullable();

            $table->boolean('pagamentorealizado');
            $table->boolean('entregarealizada');
            $table->string('tipopagamento');

            $table->integer('saleId')->unsigned();
            $table->timestamps();
            $table->foreign('saleId')->references('id')->on('sales');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrega_sales');
    }
}
