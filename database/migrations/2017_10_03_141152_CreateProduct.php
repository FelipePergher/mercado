<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeProduto')->unique();
            $table->float('qntEstoque',16,2);
            $table->string('imagem');
            $table->string('tipoProduto');
            $table->float('preco',16,2);
            $table->string('codProduto')->unique();
            $table->string('unidadeProduto');
            $table->string('composicao');
            $table->string('peso');
            $table->integer('glutem')->nullable();
            $table->integer('lactose')->nullable();
            $table->string('categoria');

            $table->string('valorCalorico')->nullable();
            $table->string('carboidratos')->nullable();
            $table->string('proteinas')->nullable();
            $table->string('gordurasT')->nullable();
            $table->string('gordurasS')->nullable();
            $table->string('colesterol')->nullable();
            $table->string('fibraAlimentar')->nullable();
            $table->string('calcio')->nullable();
            $table->string('ferro')->nullable();
            $table->string('sodio')->nullable();
            $table->integer('promocao')->nullable();
            $table->float('descontoPromocao')->nullable();
            $table->string('demaisInformacoes')->nullable();
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
