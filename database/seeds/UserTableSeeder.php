<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {
       User::create([
            'nome' => 'admin',
            'sobrenome' => 'admin',
            'email'=>'admin@admin.com.br',
            'cpf'=>'11111111111',
            'rg'=>'1.111.111',
            'telefone'=>'1111111111',
            'celular'=>'11111111111',
            'rua'=>'Videira',
            'bairro'=>'Centro',
            'complemento'=>'Casa',
            'numero'=>'111',
            'cep'=>'89560000',
            'datanascimento'=>'1111-11-11',
            'password'=>bcrypt('d3v3l0pm3nt'),
            'role' => 'gerente'
        ]);
    }
}
