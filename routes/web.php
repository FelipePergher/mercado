<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/home', 'HomeController@index');
Route::get('/productsales/{categoria}', 'ProductSalesController@getProductsbyCategory')->name('getProductsbyCategory'); 
Route::get('/products', 'ProductSalesController@getProductsbyName')->name('getProductsbyName'); 
Route::get('/productsalesdetails/{id}', 'ProductSalesController@getProductsDetails')->name('getProductsDetails');
Route::get('/showClient/{id}', 'ClientController@showClient')->name('showClient');
Route::get('/sale', 'ProductSalesController@sale')->name('sale'); 

Route::get('/salesClient/{id}', 'ProductSalesController@salesClient')->name('salesClient');
Route::get('/salesClient/show/{id}', 'ProductSalesController@showSalesClient')->name('showSalesClient');
Route::get('/undeliveredSales', 'ProductSalesController@undeliveredSales')->name('undeliveredSales'); 
Route::get('/unpaidSales', 'ProductSalesController@unpaidSales')->name('unpaidSales'); 

Route::post('/updateEntrega/{id}', 'ProductSalesController@updateEntrega')->name('updateEntrega'); 

Route::get('/client/{id}/edit', 'ClientController@edit')->name('editClient');
Route::get('/product/{id}/edit', 'ProductController@edit')->name('editProduct');
Route::get('/user/{id}/edit', 'UserController@edit')->name('editUser');

Route::post('/storeSale', 'ProductSalesController@storeSale')->name('storeSale'); 
Route::post('/updateImage', 'ProductController@updateImage')->name('updateImage'); 

Route::post('/product/promocao/{id}', 'ProductController@updatePromotion')->name('updatePromotion'); 

Route::post('/salesClient/savePayment', 'ProductSalesController@savePayment')->name('savePayment'); 
Route::post('/salesClient/saveDelivery', 'ProductSalesController@saveDelivery')->name('saveDelivery'); 

Route::resource('user','UserController');
Route::resource('product','ProductController');
Route::resource('client','ClientController');
